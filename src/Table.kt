/**
 * Determines the table
 */
class Table(p: CharsProbabilities, inpN: Int) {
    private var N = 0
    private var h = 0.0
    private val probability: CharsProbabilities
    private val map = LinkedHashMap<Int, Double>()

    init {
        N = inpN
        probability = p.makeNewCharacters(N)
        probability.sort()
        probability.makeH()
        var n = 0
        var k = 1
        while (k < probability.size()) { // form size of table
            k *= 2
            n++
        }
        map.put(n, 0.0) // put numbered Double values
        h = probability.h / N //
        makeTable(n)
    }

    /**
     *
     */
    private fun makeTable(nInp: Int) {
        var n = nInp
        var nDivN: Double
        do {
            n--
            val lost = probability.size() - Math.pow(2.0, n.toDouble()).toInt()
            val pErr = probability.countPe(lost)
            map.put(n, pErr)
            nDivN = n.toDouble() / N.toDouble()
        } while (nDivN > h)
    }

    /**
     * Output
     * Firstly prints probability. Then prints out table with n, R in comparison with H and
     * probability of error
     */
    override fun toString(): String {
        var result = ""
        println(probability)
        result += "Letters =  $N\n"
        map.keys.forEach{
            val nDivN = it / N.toDouble()
            result += if(it != map.size) "n = $it| R = ${String.format("%.4f", nDivN)} > h = ${String.format("%.4f", h)}| P(err) = ${String.format("%.4f", map[it])}\n"
            else "n = $it| R = ${String.format("%.4f", nDivN)} < h = ${String.format("%.4f", h)}| P(err) = ${String.format("%.4f", map[it])}\n"
        }
        return result
    }
}