/**
 * Class describes the scope of chars with probabilities and necessary operations
 */
class CharsProbabilities(str: String) {
    var h = 0.0
    private var charProb = mutableListOf<Element>()
    init {
        for (i in 0 until str.length) {
            add(str[i].toString())
        }
        divide(str.length)
    }

    /**
     * Count entropy
     */
    fun makeH() = charProb.forEach { h += -(it.prob * (Math.log10(it.prob) / Math.log10(2.0))) }

    /**
     * Make new [charProb] through [recursive] function
     */
    fun makeNewCharacters(n: Int): CharsProbabilities {
        val res = CharsProbabilities("")
        recursive(n, StringBuffer(), res)
        return res
    }

    /**
     *
     */
    fun recursive(n: Int, stringBuffer: StringBuffer, res: CharsProbabilities) {
        if (n == 1) {
            for (i in charProb.indices) {
                stringBuffer.append(charProb[i].chars)
                var probability = 1.0
                for (j in 0 until stringBuffer.length) {
                    val tmp = stringBuffer[j].toString()
                    val it = charProb.listIterator()
                    while (it.hasNext()) {
                        val tmpElem = it.next()
                        if (tmpElem.chars == tmp) {
                            probability *= tmpElem.prob
                            break
                        }
                    }
                }
                res.addBoth(stringBuffer.toString(), probability)
                stringBuffer.deleteCharAt(stringBuffer.length - 1)
            }
        } else {
            for (i in charProb.indices) {
                stringBuffer.append(charProb[i].chars)
                recursive(n - 1, stringBuffer, res)
                stringBuffer.deleteCharAt(stringBuffer.length - 1)
            }
        }
    }

    /**
     * If there's no such symbol [s], we add current with its probability [p]
     */
    private fun addBoth(s: String, p: Double) {
        if (!charProb.any { it.chars == s }) { // faster than usual cycle check
            charProb.add(Element(s, p))
        }
    }

    /**
     * If scope of chars [str] exists, increment probability, else add scope of chars
     */
    fun add(str: String) {
        charProb.forEach {
            if(it.chars == str) {
                it.prob++
                return
            }
        }
       charProb.add(Element(str, 1.0))
    }

    /**
     * Divide all probabilities in scope on [value]
     */
    private fun divide(value: Int) = charProb.forEach { it.prob /= value.toDouble() }

    /**
     * Getter for size of probability array
     */
    fun size() = charProb.size

    /**
     * Sorts [Element]s in scope by growth
     */
    fun sort() {
        charProb.sortWith(Comparator { t1: Element, t2: Element ->
            t1.prob.compareTo(t2.prob)
        })
    }

    /**
     * Count probability of error
     */
    fun countPe(lost: Int): Double {
        var res = 0.0
        for(i in 0 .. lost) res += charProb[i].prob
        return res
    }

    /**
     * Output
     */
    override fun toString(): String {
        val res = StringBuilder()
        for (i in charProb.indices) {
            res.append("${charProb[i].chars} ${String.format("%.5f", charProb[i].prob)} | ")
        }
        return res.toString()
    }
}

//Pe count probable
//
//        (0 .. lost).forEach { // Int range
//            res += charProb[it].prob
//        }