fun main(args: Array<String>) {
    val probability = CharsProbabilities("CHERNENKOV ALEKSANDR")

    println(probability)

    val tableFor1 = Table(probability, 1)

    println(tableFor1)

    val tableFor2 = Table(probability, 2)

    println(tableFor2)

    val tableFor3 = Table(probability, 3)

    println(tableFor3)

    val tableFor4 = Table(probability, 4)

    println(tableFor4)
}